
## Demo

- [Dashboard Page](http://localhost:3000/)
- [Users Page](http://localhost:3000/customers)
- [Products Page](http://localhost:3000/products)
- [Register Page](http://localhost:3000/register)
- [Login Page](http://localhost:3000/login)
- [Account Page](http://localhost:3000/account)
- [Settings Page](http://localhost:3000/settings)

## Free Figma Community File
 - [Duplicate File](https://www.figma.com/community/file/1039837897183395483/Devias-Dashboard-Design-Library-Kit)


We also have a pro version of this product which bundles even more pages and components if you want to save more time and design efforts :)

| Free Version (this one)  | 

## Quick start

- Make sure your NodeJS and npm versions are up to date for `React 17`

- Install dependencies: `npm install` or `yarn`

- Start the server: `npm run dev` or `yarn dev`

- Views are on: `localhost:3000`

## File Structure

Within the download you'll find the following directories and files:

```
material-kit-react

┌── .eslintrc.json
├── .gitignore
├── CHANGELOG.md
├── jsconfig.json
├── LICENSE.md
├── package.json
├── README.md
├── public
└── src
	├── __mocks__
	├── components
	├── icons
	├── theme
	├── utils
	└── pages
		├── 404.js
		├── _app.js
		├── _document.js
		├── account.js
		├── customers.js
		├── index.js
		├── login.js
		├── products.js
		├── register.js
		└── settings.js
```
