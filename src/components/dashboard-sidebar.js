import { useEffect } from "react";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import {
  Box,
  Divider,
  Drawer,
  useMediaQuery,
} from "@mui/material";
import SettingsIcon from "@mui/icons-material/Settings";
import LogoutIcon from "@mui/icons-material/Logout";
import { NavItem } from "./nav-item";
import CircleNotificationsIcon from "@mui/icons-material/CircleNotifications";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import MenuBookOutlinedIcon from "@mui/icons-material/MenuBookOutlined";
import BusinessCenterOutlinedIcon from "@mui/icons-material/BusinessCenterOutlined";
import CorporateFareOutlinedIcon from "@mui/icons-material/CorporateFareOutlined";
import RamenDiningOutlinedIcon from "@mui/icons-material/RamenDiningOutlined";
import GroupOutlinedIcon from "@mui/icons-material/GroupOutlined";
import AdminPanelSettingsOutlinedIcon from "@mui/icons-material/AdminPanelSettingsOutlined";

const items = [
  {
    href: "/",
    icon: <HomeOutlinedIcon fontSize="small" />,
    title: "Dashboard",
  },
  {
    href: "/admissionForm",
    icon: <MenuBookOutlinedIcon fontSize="small" />,
    title: "Education",
  },
  {
    href: "/office",
    icon: <BusinessCenterOutlinedIcon fontSize="small" />,
    title: "Office",
  },
  {
    href: "/residence",
    icon: <CorporateFareOutlinedIcon fontSize="small" />,
    title: "Residence",
  },
  {
    href: "/meal",
    icon: <RamenDiningOutlinedIcon fontSize="small" />,
    title: "Meal",
  },
  {
    href: "/customers",
    icon: <GroupOutlinedIcon fontSize="small" />,
    title: "Teachers",
  },
  {
    href: "/customers",
    icon: <AdminPanelSettingsOutlinedIcon fontSize="small" />,
    title: "Admin",
  },
  {
    href: "/notifications",
    icon: <CircleNotificationsIcon fontSize="small" />,
    title: "Notifications",
  },
  {
    href: "/settings",
    icon: <SettingsIcon fontSize="small" />,
    title: "Settings",
  },
  {
    href: "/logout",
    icon: <LogoutIcon fontSize="small" />,
    title: "Logout",
  },
];

export const DashboardSidebar = (props) => {
  const { open, onClose } = props;
  const router = useRouter();
  const lgUp = useMediaQuery((theme) => theme.breakpoints.up("lg"), {
    defaultMatches: true,
    noSsr: false,
  });

  useEffect(
    () => {
      if (!router.isReady) {
        return;
      }

      if (open) {
        onClose?.();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [router.asPath]
  );

  const content = (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          height: "100%",
        }}
      >
        <div>
          <Box sx={{ p: 3 }}>
            <img
              alt="Under development"
              src="/static/logo.svg"
              style={{
                marginTop: 50,
                display: "inline-block",
                maxWidth: "100%",
                width: 560,
              }}
            />
          </Box>
        </div>
        <Divider
          sx={{
            borderColor: "white",
            my: 3,
            boxShadow: "0px 1px 5px rgb(100 116 139 / 12%)"
          }}
        />
        <Box sx={{ flexGrow: 1 }}>
          {items.map((item) => (
            <NavItem
              key={item.title}
              icon={item.icon}
              href={item.href}
              title={item.title}
            />
          ))}
        </Box>
        <Divider sx={{ borderColor: "#2D3748" }} />
      </Box>
    </>
  );

  if (lgUp) {
    return (
      <Drawer
        anchor="left"
        open
        PaperProps={{
          sx: {
            backgroundColor: "neutral.900",
            color: "#FFFFFF",
            width: 280,
          },
        }}
        variant="permanent"
      >
        {content}
      </Drawer>
    );
  }

  return (
    <Drawer
      anchor="left"
      onClose={onClose}
      open={open}
      PaperProps={{
        sx: {
          backgroundColor: "neutral.900",
          color: "#FFFFFF",
          width: 280,
        },
      }}
      sx={{ zIndex: (theme) => theme.zIndex.appBar + 100 }}
      variant="temporary"
    >
      {content}
    </Drawer>
  );
};

DashboardSidebar.propTypes = {
  onClose: PropTypes.func,
  open: PropTypes.bool,
};
