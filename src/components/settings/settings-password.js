import { useState } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
} from "@mui/material";


export const SettingsPassword = (props) => {
  const [values, setValues] = useState({
    student_id: "",
  });

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <form {...props}>
      <Card sx={{ p: 10 }} id="sCard">
        <CardHeader subheader="" title="ছাত্র ভর্তি" />
        <CardContent sx={{ maxWidth: 500 }}>
          <TextField
            fullWidth
            label="পুরাতন ছাত্রের আইডি"
            margin="normal"
            name="student_id"
            onChange={handleChange}
            type="text"
            value={values.student_id}
            variant="outlined"
          />
        </CardContent>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 2,
          }}
        >
          <Button color="primary" variant="contained" size="large">
            এগিয়ে চলুন ➞
          </Button>
        </Box>

        
        <Divider className="or">অথবা</Divider>
        

        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 2
          }}
        >
          <Button color="success" variant="contained" size="large">
            নতুন ছাত্র ভর্তি
          </Button>
        </Box>
      </Card>
    </form>
  );
};
